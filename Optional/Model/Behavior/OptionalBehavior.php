<?php

class OptionalBehavior extends ModelBehavior {

	private $optionValueModelName = 'OptionValue';
	private $optionModelName = 'Option';

	private $optionValueModel;
	private $optionModel;

	public function setup(Model $Model, $settings = array()) {

	    if (!isset($this->settings[$Model->alias])) {
	        $this->settings[$Model->alias] = array(
	            'type' => null,
	        );
	    }

	    $this->settings[$Model->alias] = array_merge(
	    	$this->settings[$Model->alias], 
	    	(array)$settings
	    );

	    // Ensure this is set to run BEFORE containable.
		$this->settings['priority'] = 5;

		$Model->bindModel( 
			array(
				'hasMany' => 
					array(
						'OptionValue' => 
							array(
								'className' => 'Optional.OptionValue'
							)
					)
				), 
			false );

		$this->optionValueModel = $Model->{$this->optionValueModelName};
		$this->optionModel 	= $Model->{$this->optionValueModelName}->{$this->optionModelName};

 	}

	public function beforeValidate(Model $Model, $options = array()){
		$Model->validate = array();
		return true;
	}

	public function beforeFind(Model $Model, $query){

		$Model->contain( $this->optionValueModel->alias . '.' . $this->optionModel->alias);
		/* It should only ADD contained properties, not just be explicit. */

		return $query;

	}

	public function afterFind(Model $Model, $results, $primary = false){

		foreach($results as $key => $value){

			foreach($value[ $this->optionValueModel->alias ] as $optionValue){
				$results[$key][$Model->alias][ $optionValue[ $this->optionModel->alias ][ $this->optionModel->displayField ] ] = $optionValue['value'];
			}

			if(isset($value[ $this->optionValueModel->alias ])){
				unset($results[$key][ $this->optionValueModel->alias ]);
			}
		}

		return $results;
	}

	public function beforeSave(Model $Model, $options = array()){
		// Remove OptionValue array here to stop manual saving.
		return true;
	}



	public function afterSave(Model $Model, $created, $options = array() ){
		
		$fieldsNotInOriginalModel = array_diff(
			array_keys( $Model->data[$Model->alias] ), 
			array_keys( $Model->schema() )
		);

		$type = $this->settings[$Model->alias]['type'];

		$options = $this->optionModel->getAllByType( $type );
		$values = $Model->{$this->optionValueModel->alias}->getAllByType( $type );

		$optionValues = array();

		foreach($fieldsNotInOriginalModel as $field){

			$option = Hash::extract($options, '{n}.' . $this->optionModel->alias . '[' . $this->optionModel->displayField . '=/' . $field . '/]');

			if( !$option || empty($option) ){
				continue;
			}

			$optionId = $option[0]['id']; // Always the first result;

			$modelforeignKey 	= Inflector::singularize($Model->table) . '_id';
			$optionForeignKey 	= Inflector::singularize($this->optionModel->table) . '_id';

			$displayField = $this->optionValueModel->displayField;

			$tmpArray =  array(
				$displayField 		=> $Model->data[$Model->alias][$field],
				$modelforeignKey 	=> $Model->id,
				$optionForeignKey 	=> $optionId
			);

			$optionValueId = Hash::extract($values, '{n}.' . $this->optionValueModel->alias . '[' . $optionForeignKey .'=' . $optionId . '].' . $this->optionValueModel->primaryKey);
			if($optionValueId){
				$tmpArray[ $this->optionValueModel->primaryKey ] = $optionValueId;
			}

			$optionValues[] = $tmpArray;
		}


		// This HAS to be called because CakePHP wont allow you to modify related data
		// Even though this is a valid reason to.
		// https://github.com/cakephp/cakephp/issues/1765
		
		$Model->OptionValue->saveAll( $optionValues );

		return true;
	}

}