<?php

class OptionValue extends AppModel {

	public $displayField = 'value';

	public $belongsTo = array(
		'Option' => array(
			'className' => 'Optional.Option'
		)

	);

	public function getAllByType($type){
		$result = $this->find('all', array(
			'contain' => array(
				$this->Option->alias
			),
			'conditions' => array(
				'Option.type' => $type
			)
		));

		return $result;
	}
}