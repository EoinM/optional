<?php 

class Option extends AppModel {
 
	public function getAllByType( $type ){
		return $this->find('all', array('conditions' => array('type' => $type)));
	}

}